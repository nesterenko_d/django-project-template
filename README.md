## How to install

```
bash
django-admin startproject \ 
--template=https://gitlab.com/craft-api/django-project-template/-/archive/master/django-project-template-master.zip \
--name=.env.example \
-- extension=py,sh \
project_name .
```

If you get an SSL error, then download the zip file and put path to the zip file after --template=: 

```
bash
django-admin startproject \ 
--template=~/Downloads/django-project-template-master.zip \
--name=.env.example \
project_name .
```

Or simply, download a zip file of the project and replace template context variables


## Run with Virtual Environments


Create virtual environments
```
bash
virtualenv --python=python3.7 venv
source venv/bin/activate
```


Install pre-configured requirements
```
bash
pip install -r requirements.txt
```

Create .env file in src directory
```bash
touch src/.env
```

Add config to the file (see `.env.example` file)

Add an environment variable to read from `.env` file

```
bash
export DJANGO_READ_DOT_ENV_FILE=true
```

## Run with Docker Compose

```
docker-compose -f docker-compose.yml up
```

or

```
export COMPOSE_FILE=docker-compose-dev.yml
docker-compose up
```