from rest_framework import permissions

from drf_yasg.views import get_schema_view
from drf_yasg import openapi


api_information = openapi.Info(
    title="{{ project_name }} API",
    default_version='v1',
)

schema_view = get_schema_view(
    api_information,
    public=True,
    permission_classes=(permissions.AllowAny,)
)
