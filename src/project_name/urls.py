from django.urls import path, re_path
from .swagger_view import schema_view


swagger_urlpatterns = [
    re_path(r'^swagger(?P<format>\.json|\.yaml)$', schema_view.without_ui(), name='schema-json-yml'),
    path('swagger/', schema_view.with_ui('swagger'), name='schema-swagger-ui'),
]


urlpatterns = [

]

urlpatterns += swagger_urlpatterns
